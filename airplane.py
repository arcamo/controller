import pygame
import serial

con = serial.Serial('/dev/ttyUSB1', 115200)
try:
    con.open()
except Exception as ex:
    print ex

pygame.init()
clock = pygame.time.Clock()
pygame.joystick.init()

joystick_count = pygame.joystick.get_count()
if joystick_count == 0:
    print('Insert a Joystick')
    exit()
if joystick_count == 1:
    joystick = pygame.joystick.Joystick(0)
    joystick.init()
else:
    print('Not Implemented Yet')
    exit()


def map_value(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


def set_with_map(name, in_min=-1.0, in_max=1.0, out_min=0.0, out_max=180.0, prepare=lambda x: x):
    def send_cmd(value):
        value = prepare(value)
        value = map_value(value, in_min, in_max, out_min, out_max)
        cmd = '?%s=%d\n' % (name, int(value))
        print(cmd)
        con.write(cmd)

    return send_cmd


joy_map = {
    '4': set_with_map('motor', 0.0, 1.0, 40, 120, prepare=abs),
    '3': set_with_map('elevator'),

    '0': set_with_map('aileron', -1.0, 1.0, 180, 0),
    '1': set_with_map('rudder'),
}

done = False
while done is False:
    for event in pygame.event.get():
        if event.type == pygame.JOYAXISMOTION:
            joy_map.get(str(event.axis), lambda x: x)(event.value)
